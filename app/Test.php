<?php

namespace App;

use App\Models\CarBrand;
use App\Repositories\Contracts\TestDataRepositoryInterface;
use App\Models\Person;

class Test implements TestInterface
{
    protected TestDataRepositoryInterface $testDataRepository;

    public function __construct(TestDataRepositoryInterface $testDataRepository)
    {
        $this->testDataRepository = $testDataRepository;
    }

    public function returnFullNameOfPeopleWithGender(string $gender): array
    {
        $rawPeople = $this->testDataRepository->getPeople();
        $people = [];
        foreach ($rawPeople as $value) {
            $person = new Person($value['last-name'], $value['first-name'], $value['gender']);
            array_push($people, $person);
        }

        // For each Person in people check if gender is the gender specified, if so return full name, if not remove from array.
        $result = collect($people)->map(function ($person) use ($gender) {
            if ($gender == $person->getGender()) {
                return  $person->getFirst_Name()  . " " . $person->getLast_name();
            }
        })->reject(function ($person) {
            return empty($person);
        });

        return $result->toArray();
    }

    public function returnAllBrandNamesWithModelContaining(string $string): array
    {
        $rawCarBrands = $this->testDataRepository->getCars();
        $cars = [];

        // The JSON is a little weird, because brands is not a list. So I use the key as the name of the brand. And the value is the list of models
        foreach ($rawCarBrands['brands'] as $key => $value) {
            $car = new CarBrand($key, $value);
            array_push($cars, $car);
        }

        $result = collect($cars)->map(function ($car) use ($string) {
            foreach ($car->getModels()['models'] as $model) {
                //strpos where match is at index 0 returns 0. Which gets casted if you just use if(). Thats why I used !==, to prevent casting. 
                if (strpos($model, $string) !== false) {
                    return  $car->getBrand_name();
                }
            }
        })->reject(function ($car) {
            return empty($car);
        });
        return $result->toArray();
    }

    public function returnSumAllNumbersGreaterOrEqualThan(int $inputNumber): int
    {
        $numbers = $this->testDataRepository->getNumbers();
        // For each number check if it is higher or equal than the number specified. If it is add it to result.
        $result = collect($numbers)->filter(function ($number) use ($inputNumber) {
            if ($number >= $inputNumber) {
                return $number;
            }
        });
        return array_sum($result->toArray());
    }

    public function returnPhoneNumberEndingIn(string $string): array
    {
        $phoneNumbers = $this->testDataRepository->getPhoneNumbers();
        // For each phonenumber check if it ends with the string specified. If it is add it to the result.
        $result = collect($phoneNumbers)->filter(function ($phoneNumber) use ($string) {
            if (str_ends_with($phoneNumber, $string)) {
                return $phoneNumber;
            }
        });
        return $result->toArray();
    }
}
