<?php

namespace App\Models;

class Person
{

    function __construct($last_name, $first_name, $gender)
    {
        $this->last_name = $last_name;
        $this->first_name = $first_name;
        $this->gender = $gender;
    }

    protected string $last_name;
    protected string $first_name;
    protected string $gender;

    /**
     * Get the value of last_name
     */
    public function getLast_name()
    {
        return $this->last_name;
    }

    /**
     * Get the value of first_name
     */
    public function getFirst_name()
    {
        return $this->first_name;
    }

    /**
     * Get the value of gender
     */
    public function getGender()
    {
        return $this->gender;
    }
}
