<?php

namespace App\Models;

class CarBrand
{

    function __construct($brand_name, $models)
    {
        $this->brand_name = $brand_name;
        $this->models = $models;
    }

    protected string $brand_name;
    protected array $models;

    /**
     * Get the value of brand_name
     */
    public function getBrand_name()
    {
        return $this->brand_name;
    }

    /**
     * Get the value of models
     */
    public function getModels()
    {
        return $this->models;
    }
}
